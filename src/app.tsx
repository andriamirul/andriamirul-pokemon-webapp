import React from 'react';
import { ApolloClient, ApolloProvider, createHttpLink, InMemoryCache } from '@apollo/client';

const client = new ApolloClient({
  uri: 'https://graphql-pokeapi.vercel.app/api/graphql',
  cache: new InMemoryCache()
});
export function rootContainer(container: React.Component) {
  return <ApolloProvider client={client}>{container}</ApolloProvider>;
}
