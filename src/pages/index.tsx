import React from 'react';
import { Card, Layout,Row } from 'antd';
import { gql, useQuery,ApolloProvider } from '@apollo/client';
import { ApolloClient, InMemoryCache } from '@apollo/client';
import { history } from 'umi';
import './index.less';
import Pokemonheader from './header';
import Cookies from 'universal-cookie';
const cookies = new Cookies();
// import Dashboard from './pokemon-detail';
const client = new ApolloClient({
  uri: 'https://graphql-pokeapi.vercel.app/api/graphql',
  cache: new InMemoryCache()
});
export default (): React.ReactNode => {
const GET_POKEMONS = gql`
  query pokemons($limit: Int, $offset: Int) {
    pokemons(limit: $limit, offset: $offset) {
      count
      next
      previous
      status
      message
      results {
        id
        url
        name
        image
      }
    }
  }
`;
const gqlVariables = {
    limit: 100,
    offset: 0,
  };
  const { loading, error, data } = useQuery(GET_POKEMONS,{variables: gqlVariables,});
  
  if (loading) return (<img style={{position:"fixed",top:"50%",left:"50%",transform:"translate(-50%, -50%)"}} src="https://i.gifer.com/origin/28/2860d2d8c3a1e402e0fc8913cd92cd7a_w200.gif" />);
  if (error) return `Error! ${error.message}`;
  const render_total = (name : any) => {
    var cookies_name = JSON.stringify(name);
    var the_cookie = cookies.get(cookies_name);
    if(the_cookie){
      return 'owned : '+the_cookie.jumlah;
    }else{
      return 'owned : 0';
    }
  }
  
  return (
    <ApolloProvider client={client}>
    <Layout>
    <Pokemonheader/>
    <Row className="row_poke_list">
    {data.pokemons.results.map((result: any) => (
      <div className="col">
        <div className="col-md-3 col-sm-6 mb-5">
      <a onClick={()=>{
        history.push('/detail/?name='+result.name);
      }} id="pokemon_card">
                  <Card className="card_container" key={result.name}>
                    <div className="card_content">
                      <img className="pokemon_img" src={result.image}/>
                      <p className="pokemon_name">{result.name}</p>
                      <p className="pokemon_owned" id="cok">{render_total(result.name)}</p>
                    </div>
                </Card>
      </a>
        </div>
        </div>
    ))}
    </Row>
    </Layout>
    </ApolloProvider>
  );
};
