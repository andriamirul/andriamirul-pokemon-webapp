import React,{Component} from 'react';
import { Card, Layout,Row,Col, Form, Input, Menu, Image } from 'antd';
const {Header} = Layout;
import './header.less';
import { history } from 'umi';


class Pokemonheader extends Component {
render(){
    return(
       <Header className="pokemon_header" >
       <Menu theme="dark" mode="horizontal">
         <a onClick={()=>{
         history.push('/');
       }}>
           <img className="logo" src={"https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/International_Pok%C3%A9mon_logo.svg/1280px-International_Pok%C3%A9mon_logo.svg.png"}/>
         </a>
         <Menu.Item key="1" style={{float:"right"}}>
           <a onClick={()=>{
             history.push('/detail/mypokemon/')
           }}>
             <img className="my-pokemon" src="https://i.imgur.com/awfSOuB.png" />
           </a>
         </Menu.Item>
       </Menu>
     </Header> 
    );
}
};

export default Pokemonheader;
