import React,{Component} from 'react';
import { EditOutlined } from '@ant-design/icons';
import { Card, Layout,Row,Form, Input, Alert, Modal} from 'antd';
import { gql, useQuery,ApolloProvider } from '@apollo/client';
import { ApolloClient, InMemoryCache } from '@apollo/client';
import { history } from 'umi';
import '../../index.less';
import Pokemonheader from '../../header';
import Cookies from 'universal-cookie';
import { result } from 'lodash';
import Submitter from '@ant-design/pro-form/lib/components/Submitter';
const cookies = new Cookies();
// import Dashboard from './pokemon-detail';
const client = new ApolloClient({
  uri: 'https://graphql-pokeapi.vercel.app/api/graphql',
  cache: new InMemoryCache()
});
const {Header} = Layout;
export default (): React.ReactNode => {
const GET_POKEMONS = gql`
  query pokemons($limit: Int, $offset: Int) {
    pokemons(limit: $limit, offset: $offset) {
      count
      next
      previous
      status
      message
      results {
        id
        url
        name
        image
      }
    }
  }
`;
const gqlVariables = {
    limit: 100,
    offset: 0,
  };
  const { loading, error, data } = useQuery(GET_POKEMONS,{variables: gqlVariables,});
  const { info } = Modal;
  
  if (loading) return (<img style={{position:"fixed",top:"50%",left:"50%",transform:"translate(-50%, -50%)"}} src="https://i.gifer.com/origin/28/2860d2d8c3a1e402e0fc8913cd92cd7a_w200.gif" />);
  if (error) return `Error! ${error.message}`;
  function render_my_pokemon() {
    var pokemon_data = [];
    var keys = Object.keys(data.pokemons.results);
    var len = keys.length;
    for(var i = 0 ; i<len ; i++){
      var cookies_name = JSON.stringify(data.pokemons.results[i].name);
      var the_cookie = cookies.get(cookies_name);
      if(the_cookie){
        if(data.pokemons.results[i].name == the_cookie.name){
            var jumlah_pokemon = the_cookie.jumlah
              // var z=2;
              const all_cookies = cookies.getAll()
              var keyz = Object.keys(all_cookies);
              for(var y = -1; y<jumlah_pokemon;y++){
                  var z=y+1;
                  var pokemon_entity = the_cookie.name+y;
                  var entity = (cookies.get(pokemon_entity));
                  if(entity==undefined){
                    y++;
                    z++;
                    var pokemon_entity = the_cookie.name+y;
                    var entity = (cookies.get(pokemon_entity));
                    jumlah_pokemon++;

                  }
                  if(entity){
                    entity.image=data.pokemons.results[i].image;
                    pokemon_data.push({ _id: entity._id, ras : the_cookie.name, image : data.pokemons.results[i].image, jumlah : the_cookie.jumlah, costum_name:entity.costum_name});
                }

              }

            }

          }   
      }
    return pokemon_data;

  }
  const my_pokemon = render_my_pokemon();
  const release = async (name: any, ras: any, jumlah_pokemon : any)=>{
    var new_jumlah_pokemon = jumlah_pokemon-1;
    const detail = ({
      name : ras,
      jumlah : new_jumlah_pokemon

    });

    cookies.remove(name , {path: '/'});
    cookies.set(JSON.stringify(ras),detail);
    if(cookies.get(JSON.stringify(ras)).jumlah==0){
      cookies.remove(JSON.stringify(ras),{path:'/'});
    }
    return history.push('/');
  }
  function changename(image:any,ras:any,name:any,id:any,costum_name:any){
    const image_failed = <Form id="my_poke_formid" className="my_poke_form"><img className="my_poke_image" src={image} /><input type="text" id="pokemon_custom_name" placeholder={costum_name} className="my_pokemon_name" /></Form>
    var button = <EditOutlined />
    showdata(image_failed,ras,name,button,id);
  }
  function showdata(image:any,ras:any,name:any,button:any,id:any) {

    info({
      title:'Treat your pokemon by changing it name' ,
      content:image ,
      cancelButtonProps : {
        hidden : true
      },
      okText : button,
      onOk() {
        var pokemon_name = document.getElementById("pokemon_custom_name");
        var form = document.getElementById("my_poke_formid");
        const all_cookies = cookies.getAll();
        var keys = Object.keys(all_cookies);
        var len = keys.length;
        var poke_ray: any[] = [];
        var decided = false;
        var y =0;
        keys.forEach((all_cookie:any)=>{
          poke_ray.push(all_cookie);
        })
        for(var i = 0; i < len ; i++ ){
            if(all_cookies[poke_ray[i]].costum_name ==pokemon_name.value ){
              decided = true;
            }
        }
        const more_detail = ({
          _id : id,
          ras : ras,
          costum_name : pokemon_name.value,
        })
        if(decided == true){
          // var z = document.getElementById('alert_exist');
          // z.style.display="block";

        }else if(decided == false){
          cookies.set(name,more_detail);
          form.submit();

          }
        
        }
    
    });
  }
  return (
    <ApolloProvider client={client}>
    <Layout>

    <Pokemonheader/>
    <div id="alert_exist"   style={{display:"none"}}><Alert message="Pokemon name already in use, please use another name" type="error" showIcon /></div>

    <Row className="row_poke_list">

    {my_pokemon.map((result: any) => (
      <div className="col">
        <div className="col-md-3 col-sm-6 mb-5">
      
                  <Card className="card_container" >
                        <a onClick={()=>{changename(result.image,result.ras,result.ras+result._id,result._id,result.costum_name);
            }} id="pokemon_card">
                    <div className="card_content">
                      <img className="pokemon_img" src={result.image}/>
                      <p className="pokemon_name">{result.costum_name}</p>
                    </div>
                </a>
              <a onClick={()=>release(result.ras+result._id,result.ras,result.jumlah)} className="release_button">
                      <img style={{height:"27px",width:"27px"}} src="https://i.imgur.com/peRA8PF.png"/>
                Release
             </a>
                </Card>

        </div>
        </div>
    ))}
    </Row>
    </Layout>
    </ApolloProvider>
  );
};
