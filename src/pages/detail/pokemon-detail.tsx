import React from 'react';
import { Card, Layout,Row,Col, Form, Input, Modal } from 'antd';
import { gql, useQuery,ApolloProvider } from '@apollo/client';
import { ApolloClient, InMemoryCache } from '@apollo/client';
import './pokemon-detail.less';
import Pokemonheader from '../header';
import Cookies from 'universal-cookie';

const client = new ApolloClient({
  uri: 'https://graphql-pokeapi.vercel.app/api/graphql',
  cache: new InMemoryCache()
});
const {Header,Footer} = Layout;
const { confirm } = Modal;
export default (): React.ReactNode => {
  const cookies = new Cookies();
  var url_string = document.URL;
  var url = new URL(url_string);
  var pokemon_name = url.searchParams.get("name");
  var pokemon_name_cookie = JSON.stringify(pokemon_name);
const GET_POKEMONBYNAME = gql`
    query pokemon($name: String!) {
      pokemon(name: $name) {
        id
        name
        stats{
          base_stat
          effort
          stat{
            name
            url
            }
        
          }
        weight
        height
        sprites{
          back_default
          front_default
        }
        abilities{
          ability{
            name
          }}
          moves{
            move{
              url
              name
            }}
            types{
              slot
              type{
                name
              }
            }
        }
    }
`;
  const name = {
    name: pokemon_name
  };
  const { loading, error, data } = useQuery(GET_POKEMONBYNAME,{variables: name,});
  
  if (loading) return (<img style={{position:"fixed",top:"50%",left:"50%",transform:"translate(-50%, -50%)"}} src="https://i.gifer.com/origin/28/2860d2d8c3a1e402e0fc8913cd92cd7a_w200.gif" />);
  if (error) return `Error! ${error.message}`;
  const TYPE_COLORS = {
    bug: 'B1C12E',
    dark: '4F3A2D',
    dragon: '755EDF',
    electric: 'FCBC17',
    fairy: 'F4B1F4',
    fighting: '823551D',
    fire:'E73B0C',
    flying: 'A3B3F7',
    ghost: '6060B2',
    grass: '74C236',
    ground: 'D3B357',
    ice: 'A3E7FD',
    normal: 'C8C4BC',
    poison: '934594',
    psychic: 'ED4882',
    rock: 'B9A156',
    steel: 'B5B5C3',
    water: '3295F6'
  };
  const types = data.pokemon.types.map((result : any) => result.type.name);
  const type = data.pokemon.types.map((result : any) => result.type.name.slice(0,1).toUpperCase()+result.type.name.slice(1, result.type.name.length));
  const themeColor = `${TYPE_COLORS[types[types.length - 1]]}`;
  
  function catchPokemon(){
    if (Math.random() >= 0.5){
      showSuccess();
    }else{
      showFailed();
    }
  }
  
  const image_failed = <img className="failed_image" src="http://images4.fanpop.com/image/photos/19000000/Someone-failed-to-capture-Pikachu-pikachu-19018020-225-255.jpg" />
  const image_success = <img className="success_image" src="https://media2.giphy.com/media/FD5ovQ3QfoPgxfPYqC/giphy.gif" />
  function showFailed() {

    confirm({

      title: 'Failed! ouch, looks like it hurts',
      content: image_failed,
      cancelButtonProps : {
        hidden : true
      },
      okText : "Ok, lets throw it again",
      onOk() {

      },
    
    });
  }
  function showSuccess() {
    confirm({
      title: 'Successful! Gotta catch em all!',
      content: image_success,
      cancelButtonProps : {
        hidden : true
      },
      onOk() {
        
        if(cookies.get(pokemon_name_cookie)==null || cookies.get(pokemon_name_cookie).jumlah==0){
          const detail = ({
            name : pokemon_name,
            jumlah : 1
          });
          cookies.set(pokemon_name_cookie,detail, {path:'/'});
          const more_detail = ({
            _id : 1,
            ras : pokemon_name,
            costum_name :pokemon_name,
            image: ''
            
          })
          
          cookies.set(JSON.parse(pokemon_name_cookie)+1,more_detail, {path:'/'});
        }else{
          const all_cookies = cookies.getAll()
          var keyz = Object.keys(all_cookies);
         
              var jumlahs = parseInt(cookies.get(pokemon_name_cookie).jumlah);
              var pertambahan = jumlahs+1;
              const detail = ({
                name : pokemon_name,
                jumlah : pertambahan
      
              });
            
              cookies.set(pokemon_name_cookie,detail);
              var cadangan = pertambahan;
              for(var i =1 ; i< pertambahan ;i++){
                var pokemon_entity = JSON.parse(pokemon_name_cookie)+i;

                var entity = (cookies.get(pokemon_entity));
                if(entity == undefined){
                  i++;
                var pokemon_entity = JSON.parse(pokemon_name_cookie)+i;
                  var entity = (cookies.get(pokemon_entity));
                  pertambahan++;

                }if(entity != undefined){
                  if(entity._id == pertambahan){
                    pertambahan ++;
                  }
                  const more_detail = ({
                    _id : pertambahan,
                    ras : pokemon_name,
                    costum_name : pokemon_name,
                    image: ''
                  })
                    cookies.set(JSON.parse(pokemon_name_cookie)+pertambahan,more_detail), {path:'/'};
                }
              }
          }
          
      },
    
    });
   
  }
  return (
    <ApolloProvider client={client}>
    <Layout className="">
    <Pokemonheader/>
    <div className="pokemun_detail_layout">
        <Card className="detail_pokemon_card">
        <Row>
              <Col className="" flex={"none"}>
              <div className="imgwrap">
              <img className="pokemon_image" src={data.pokemon.sprites.front_default}/>
                <Form>
                  <Form.Item>
                    <Input className="pokemon_detail_name" value={data.pokemon.name.slice(0,1).toUpperCase()+data.pokemon.name.slice(1, data.pokemon.name.length)}></Input>
                  </Form.Item>
                </Form>
                <div className="pokemon_type_wrap">
                {types.map((result:any)=>(
                <p style={{backgroundColor:"#"+TYPE_COLORS[result]}} className="pokemon_type circle">{result.slice(0,1).toUpperCase()+result.slice(1, result.length)}</p>
                ))}
                </div>
                </div>
                </Col>
                <Col flex={"auto"}>

                {data.pokemon.stats.map((result: any) => (
                    <Row className={"stat_wrapper"}>
                    <Col flex={"120px"}>
                      <span className="title_bar">
                        {result.stat.name.slice(0,1).toUpperCase()+result.stat.name.slice(1, result.stat.name.length)}
                      </span>
                    </Col>
                    <Col className="progres_wrapper" flex={"auto"}>
                    <div className="progress">
                        <div
                          className="progress_bar"
                          role="progressbar"
                          style={{
                            width: result.base_stat+"%",
                            backgroundColor: "#"+themeColor,
                            
                          }}
                        >
                          <p style={{color:"white"}}>{result.base_stat}</p>
                        </div>
                        </div>
                      </Col>
                      
                    </Row>
                ))}  
        </Col>
            </Row>
        <hr/>

              <div className="abilities_moves_wrapper">
            <Row>
              <Col flex={"100px"}>
                <div className="name_wrapper">
                  <h3 className="abilities_noves_name">Abilities</h3>
                </div>
                </Col>
              <Col className="progres_wrapper" flex={"auto"}>
                  {data.pokemon.abilities.map((result: any) => (
                        <p style={{backgroundColor:"#"+themeColor}} className="circle ability_name">{result.ability.name.slice(0,1).toUpperCase()+result.ability.name.slice(1, result.ability.name.length)}</p>
                    ))} 
              </Col>
              </Row>
              <Row>
              <Col flex={"100px"}>
                <div className="name_wrapper">
                  <h3 className="abilities_noves_name">Moves</h3>
                </div>  
                </Col>
              <Col className="progres_wrapper" flex={"auto"}>
                  {data.pokemon.moves.map((result: any) => (
                        <p className="moves_name circle">{result.move.name.slice(0,1).toUpperCase()+result.move.name.slice(1, result.move.name.length)} </p>
                    ))}
              </Col>
              </Row>    
              </div>
        </Card>
        </div>
        <Footer className="poke_foot">
          <div className="catch_pokemon">
            <a className="detail_button" onClick={catchPokemon}>
              <img className="ball_img" src={"https://upload.wikimedia.org/wikipedia/commons/5/53/Pok%C3%A9_Ball_icon.svg"}/>
              <div className="catch_text">CATCH</div>
            </a>
          </div>
        </Footer>
        
    </Layout>
    </ApolloProvider>
  );
};
