﻿export default [
  {
    path: '/',
    layout: false,
    component: './index',
  },
  {
    path: '/detail',
    layout: false,
    component: './detail/pokemon-detail',
  },
  {
    path: '/detail/mypokemon',
    layout: false,
    component: './detail/mypokemon/mypokemon',
  },
  
];
